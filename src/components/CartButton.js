import { Fragment, useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown, Form, FormControl, Button, Badge} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function CartButton(){
    const {user, setUser} = useContext(UserContext);
    return(
        (user.id) ?
         <Nav.Link as={NavLink} to="/mycart" exact> <button id="cartB" title=" My Cart" >   My Cart</button> </ Nav.Link>

        :
         <Nav.Link as={NavLink} to="/register" exact> <button id="cartB" title="Register">Register now!</button> </ Nav.Link>

      )  
}
