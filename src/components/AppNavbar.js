import { Fragment, useContext } from 'react';
import { Navbar, Nav, Container, NavDropdown, Form, FormControl, Button} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

// AppNavbar component
export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);
	console.log(user)
	return(
		<>
		<Navbar  collapseOnSelect bg="dark" variant="dark"  sticky="top" className="text">
		  <Container>
		    <Navbar.Brand href="/">
		    <img
		      src="https://img.icons8.com/bubbles/50/000000/cafe.png"
		      width="30"
		      height="30"
		      className="d-inline-block align-top"
		      alt="Happy Cafe"
		    />  Happy Café</Navbar.Brand>
		    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		    <Navbar.Collapse id="responsive-navbar-nav">
		      <Nav
		        className="me-auto"
		      >
		        <Nav.Link as={NavLink} to="/products" exact>Our Products </Nav.Link>
		    </Nav>
		      <Form className="d-flex">
		        <FormControl
		          type="search"
		          placeholder="Looking for something?"
		          className="me-2"
		          aria-label="Search"
		        />
		        <Button variant="danger">🔍</Button>



		      </Form>

		      <Nav className="ml-auto">
		    	{user.id && !user.isAdmin ? 
		    		<NavDropdown title="My Account" id="collapsible-nav-dropdown">
				        <NavDropdown.Item as={NavLink} to="/mycart" exact>My Cart</NavDropdown.Item>
				        <NavDropdown.Item as={NavLink} to="/orderhistory" exact>My Order History</NavDropdown.Item>
				        <NavDropdown.Divider />
				        <NavDropdown.Item as={NavLink} to="/logout" exact>Log Out</NavDropdown.Item>
			  	  </NavDropdown> 
			  		:
			  		<></>
		    	}
		    	{user.id && user.isAdmin ? 
		    	  <NavDropdown title="My Account" id="collapsible-nav-dropdown">
		    	    <NavDropdown.Item as={NavLink} to="/logout" exact>Log Out</NavDropdown.Item>
			  	  </NavDropdown> 
			  		:
			  		<></>
		    	}
    	    	{!user.id? 
    	    	<>
    	    	  <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
    	    	  <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
    		  	</>
    		  		:
    		  		<></>
    	    	}
{/*		    {user.id ?
		      <>
			      <NavDropdown title="My Account" id="collapsible-nav-dropdown">
				       {!user.isAdmin ? 
					       	<>
						        <NavDropdown.Item as={NavLink} to="/mycart" exact>My Cart</NavDropdown.Item>
						        <NavDropdown.Item as={NavLink} to="/orderhistory" exact>My Order History</NavDropdown.Item>
						        <NavDropdown.Divider />
						        <NavDropdown.Item as={NavLink} to="/logout" exact>Log Out</NavDropdown.Item>
					       	</>
					       :
					        <NavDropdown.Item as={NavLink} to="/logout" exact>Log Out</NavDropdown.Item>
				  	   }
			  	  </NavDropdown> 
			  </>
			      : 
			  <>
			      <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
			      <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>	
			  </>
		  	} */}
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
</>
	)
}