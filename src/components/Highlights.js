import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  
				  <Card.Body>
				    <Card.Title>
				    	<h2>All-Natural Delicious Ingredients</h2>
				    </Card.Title>
				    <Card.Text>
				    	Guaranteed freshness and healthiness. 
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>100% Locally-Sourced</h2>
				    </Card.Title>
				    <Card.Text>
				      Supported by local farms and small businesses.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>
				    	<h2>Affordably Healthy</h2>
				    </Card.Title>
				    <Card.Text>
				     	Delicious yet affordable.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}