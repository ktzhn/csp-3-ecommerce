//import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Error from './pages/Error';
import Products from './pages/Products';
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import OrderHistory from './pages/Orders';
import UserCart from './pages/UserCart';

import './App.css';
import { UserProvider } from './UserContext';

function App() {
  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    id: null,
    isAdmin:null
  })
  // Function for clearing localStorage on logout

  useEffect(() => {
    fetch('https://desolate-dusk-90349.herokuapp.com/users/details', {
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        })
    })
  }, [user.id])

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      id: null,
      isAdmin:null
    })
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
          <Container className="container-fluid">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/products" component={Products} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/mycart" component={UserCart} />
              <Route exact path="/orderhistory" component={OrderHistory} />
              <Route component={Error} />
            </Switch>
          </Container>
        </Router>
      </UserProvider>
  );
}

export default App;