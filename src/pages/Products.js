import { Fragment, useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import { Row, Table, Col, Modal, Form, Button } from 'react-bootstrap';
import Banner from '../components/Banner';
import CartButton from '../components/CartButton';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Products(){ 
	const { user } = useContext(UserContext);
	
	const [products, setProducts] = useState([]);
    const [prodName, setprodName] = useState("")
    const [prodDescription, setprodDescription] = useState("")
    const [prodPrice, setprodPrice] = useState(0)
    const [numberOf, setAmountOf] = useState(1)

	const [showAdd, setShowAdd] = useState(false)
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)


	useEffect(() => {
		fetch((user.isAdmin ? 'https://desolate-dusk-90349.herokuapp.com/products/all' : 'https://desolate-dusk-90349.herokuapp.com/products'))	
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "courses" state to map the data retrieved from the fetch request in several "CourseCard" components
			setProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} setProducts={setProducts}/>
					)
				})
			);
		})
	}, [user])

    const addProduct = (e, productId) => {

        e.preventDefault()

        fetch(`https://desolate-dusk-90349.herokuapp.com/products/create`, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: prodName,
            description: prodDescription,
            price: prodPrice
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === true){
        	Swal.fire({
        	    title: "New Product Created!",
        	    icon: "success",
        	    text: "Successfully created product"
        	});
            setprodName("")
            setprodDescription("")
            setprodPrice(0)
            closeAdd()
        }else{
        	Swal.fire({
        	    title: "Error",
        	    icon: "error",
        	    text: "Couldn't create product."
        	});
            closeAdd()
        }
    })
    }


const data={
                    title: "Happy Café Menu",
                    content: "",

                }

	return (
		<Fragment>
        <Banner data={data}/>
		<Row xs={1} md={(user.isAdmin ? 9 : 3)} className="mx-auto p-3 m-3">
		  {!user.isAdmin ? products : (
		  	<Fragment>
		  	<Row className="mb-5">
			  	<Col></Col>
			  	<Col>
			  	<button className="btn-success btnpress" onClick={openAdd}> Create New Order </button>
			  	</Col>
			  	<Col></Col>
		  	</Row>
		  	<Table striped bordered hover responsive>
				<thead className="bg-secondary text-white">
					<tr className="text-center">
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>						
			</Table>
			</Fragment>
		  )}
		</Row>
		{ !user.isAdmin? 		
			<CartButton />
			 : 
			 <></>}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
							<Form.Group controlId="productName">
								<Form.Label>Name:</Form.Label>
								<Form.Control type="text" placeholder="Enter product name" value={prodName} onChange={e => setprodName(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productDescription">
								<Form.Label>Description:</Form.Label>
								<Form.Control type="text" placeholder="Enter product description" value={prodDescription} onChange={e => setprodDescription(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="productPrice">
								<Form.Label>Price:</Form.Label>
								<Form.Control type="number" value={prodPrice} onChange={e => setprodPrice(e.target.value)} required/>
							</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>	
			</Modal>
		</Fragment>
	)
}