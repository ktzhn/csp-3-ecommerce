import React, { useState, useEffect, useContext} from 'react';
import { Container, Card, Accordion } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext';

export default function Orders(){
	const { user } = useContext(UserContext);
	const [listOrders, setListOrders] = useState([])


	useEffect(()=> {

		fetch(`https://desolate-dusk-90349.herokuapp.com/users/myOrders/${user.id}`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.length > 0){
				let orders = data.map((item, index)=> {
					return(
						<Card key={item._id}>
							<Accordion as={Card.Header} eventKey={index + 1} className="bg-secondary text-white">
								Order #{index + 1} - Purchased on: {item.purchasedOn}
							</Accordion>
							<Accordion eventKey={index + 1}>
								<Card.Body>
									<h6>Items:</h6>
									<ul>
									{
										item.orderProducts.map((theItem) => {
											return (
												<li key={theItem.productId}>{theItem.name} - Quantity: {theItem.numberOf}</li>
											)
										})
									}
									</ul>
									<h5 className="text-center">Total: <span className="text-success">₱{item.totalAmount}</span></h5>
								</Card.Body>
							</Accordion>
						</Card>
					)
				})
	
				setListOrders(orders)				
			}
		})
	}, [user])

	return(
		listOrders.length === 0
		?
				<h1 className="text-center p-5 m-3">No previous orders found. <Link to="/products">Check our products and order some!</Link></h1>

		:
		<Container>
			<h2 className="text-center m-3 p-2">Previous Orders</h2>
			<Accordion>
				{listOrders}
			</Accordion>
		</Container>
	)
}