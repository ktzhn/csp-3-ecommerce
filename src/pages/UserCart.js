import React, { useState, useEffect, useContext } from 'react';
import { Container,  Button,  Table, InputGroup, FormControl,  } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
export default function UserCart(){
	const [cart, setCart] = useState([])
	const [total, setTotal] = useState(0)
	const [orderRows, setorderRows] = useState([])
	const [pageRedir, setpageRedir] = useState(false)
	const { user } = useContext(UserContext);

	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, [])

	useEffect(()=> {
		let cartItems = cart.map((prod, index) => {

			return (
		      <tr key={prod.productId}>
		          <td>{prod.name}</td>
		          <td>₱{prod.price}</td>
		          <td>
				  	<InputGroup className="d-md-none">
						<FormControl type="number" min="1" value={prod.numberOf} onChange={e => orderInput(prod.productId, e.target.value)}/>
					</InputGroup>
					<InputGroup className="d-none d-md-flex w-50">
						<InputGroup>
							<Button variant="secondary" onClick={() => orderCount(prod.productId, "-")}>-</Button>
						</InputGroup>
						<FormControl type="number" min="1" value={prod.numberOf} onChange={e => orderInput(prod.productId, e.target.value)}/>
						<InputGroup>
							<Button variant="secondary" onClick={() => orderCount(prod.productId, "+")}>+</Button>
						</InputGroup>
					</InputGroup>
		          </td>
		          <td>₱ {parseFloat(prod.price)*parseFloat(prod.numberOf)}</td>
		          <td className="text-center">
		          	<Button variant="danger" onClick={() => rmvBtn(prod.productId)}>Remove</Button>
		          </td>
		      </tr>			
			)
		})


	setorderRows(cartItems)
		let result = 0
		cart.forEach((prod)=> {
			result += (parseFloat(prod.price)*parseFloat(prod.numberOf))
		})

		setTotal(result)
	}, [cart])

	const orderInput = (productId, value) => {

		let result = [...cart]

		if(value === ''){
			value = 1
		}else if(value === "0"){
			Swal.fire({
			    title: "Please input a value!",
			    icon: "error",
			    text: "You cannot order 0 of an item."
			});
			value = 1
		}

		for(let i = 0; i < cart.length; i++){
			console.log(result[i].price)
			console.log(result[i].numberOf)

			if(result[i].productId === productId){
				result[i].numberOf = parseFloat(value)
				result[i].subtotal = parseFloat(result[i].price)  * parseFloat(result[i].numberOf) 
			}
		}

		setCart(result)
		localStorage.setItem('cart', JSON.stringify(result))	
	}

	const orderCount = (productId, operator) => {

		let result = [...cart]

		for(let i = 0; i < result.length; i++){

			if(result[i].productId === productId){
				if(operator === "+"){
					result[i].numberOf += 1
					result[i].subtotal = result[i].price * result[i].numberOf
				}else if(operator === "-"){
					if(result[i].numberOf <= 1){
						Swal.fire({
						                title: "Error!",
						                icon: "error",
						                text: "Quantity cannot be zero!"
						            });
					}else{
						result[i].numberOf -= 1
						result[i].subtotal = result[i].price * result[i].numberOf
					}
				}
			}
		}

		setCart(result)
		localStorage.setItem('cart', JSON.stringify(result))
	}

	const rmvBtn = (productId) => {
		let result = [...cart]

		let cartIds = result.map((prod)=> {
			return prod.productId
		})

		result.splice([cartIds.indexOf(productId)], 1)

		setCart(result)
		localStorage.setItem('cart', JSON.stringify(result))	
	}

	const checkout = () => {

		const checkoutCart =  cart.map((prod) => {
			return {
				name: prod.name,
				productId: prod.productId,
				numberOf: prod.numberOf,
				price: prod.price
			}
		})

		fetch(`https://desolate-dusk-90349.herokuapp.com/users/checkout`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				orderProducts: checkoutCart,
				totalAmount: total
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
								Swal.fire({
				                title: "Order placed",
				                icon: "success",
				                text: "Thank you for ordering!"
				            });

				localStorage.removeItem('cart')

				setpageRedir(true)
			}else{
				Swal.fire({
				                title: "Error",
				                icon: "error",
				                text: "Unable to place order!"
				            });
			}
		})
	}

	return(
		pageRedir === true
		? <Redirect to="/orderhistory"/>
		:
		cart.length <= 0
			? <h1 className="text-center p-5 "> No products found in your cart. <Link to="/products">Check out our products and order something!</Link></h1>
			:
			<Container>
				<h2 className="text-center my-4">Your Shopping Cart</h2>
				<Table striped bordered hover responsive>
					<thead className="bg-secondary text-white">
						<tr>
							<th>Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{orderRows}
						<tr>
							<td colSpan="3">
								<Button variant="success" className="btnpress" onClick={()=> checkout()}>Checkout</Button>
							</td>
							<td colSpan="2">
								<h3>Total: ₱{total}</h3>
							</td>
						</tr>
					</tbody>						
				</Table>
			</Container>
	)
}